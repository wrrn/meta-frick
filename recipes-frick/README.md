# recipes-frick
recipes-frick provides the frick-image package that we will use to build our frick image.

## Directories
### images
images provides the frick-image which does the following:

* Add frick-specific directories (see ```frick_add_dirs()``` for
  directories that are added)
* Add frick and root user and setup their passwords
* Pull in package groups for installation (see
  ```IMAGE_INSTALL_APPEND``` the names of packages groups that are
  added )

### packagegroups
packagegroups provides the package group ```packagegroup-frick```
