# TODO Remove the root password it should only be for development
DESCRIPTION = "A console-only image with more full-featured Linux system \
functionality installed."

# IMAGE_FEATURES += "splash ssh-server-openssh dev-pkgs"

LICENSE = "MIT"

inherit distro_features_check

REQUIRED_DISTRO_FEATURES = "x11"

IMAGE_INSTALL_append = "\
    packagegroup-core-boot \
    packagegroup-core-full-cmdline \
    packagegroup-core-lsb \
    packagegroup-core-buildessential \
    packagegroup-core-x11 \
    packagegroup-core-ssh-openssh \
    packagegroup-frick \
    packagegroup-frick-openbox \
    packagegroup-frick-reporting \
    ${CORE_IMAGE_EXTRA_INSTALL} \
    "


frick_add_dirs() {
   local dirs="/uniq/data \
               /uniq/freeze \
               /uniq/flags \
               /uniq/refrigerants \
               /scratch \ 
               /scratchkeep \
               /cache \
               /etc/prime-unity/distributer \
               /etc/unity-network \
    "
   for d in $dirs; do
      mkdir -p ${IMAGE_ROOTFS}/$d
   done

}

ROOTFS_POSTPROCESS_COMMAND += "frick_add_dirs; "

inherit core-image distro_features_check
inherit extrausers

REQUIRED_DISTRO_FEATURES = "x11"
EXTRA_USERS_PARAMS = "\
         useradd -d /home/frick -m -r -o -P frick -u 0 -g 0 frick; \
         useradd -d /home/super -m -r -o -P frickecs1492 -u 0 -g 0 super; \
         usermod -P frick root; \
    "

