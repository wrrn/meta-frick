SUMMARY = "Frick Packages"
DESCRIPTION = "Frick Custom Package Group"
LICENSE = "MIT"

inherit packagegroup

PACKAGES = "\
    packagegroup-frick \
    packagegroup-frick-db \
    packagegroup-frick-boot \
    packagegroup-frick-network \
    packagegroup-frick-environment \
    packagegroup-frick-security \
    packagegroup-frick-ui \
    packagegroup-frick-ui-libs \
    "

RDEPENDS_packagegroup-frick = "${PACKAGES}"

RDEPENDS_packagegroup-frick-db = "\
    sqlite3 \
    "

RDEPENDS_packagegroup-frick-environment = "\
    profiled \
    " 

RDEPENDS_packagegroup-frick-ui = "\
    xz \
    pciutils \
    pulseaudio \
    cairo \
    nss \
    zlib \
    gstreamer1.0-libav \
    cups \
    gconf \
    pango \
    libdrm \
    gtk+ \
    gconf \
    libgnome-keyring \   
    liberation-fonts \
    ttf-bitstream-vera \
    cantarell-fonts \
    fontconfig \
    "

# Missing: xextproto
# Mappings:
#   libxss => libxscrnsaver
RDEPENDS_packagegroup-frick-boot = "\
    psplash \
"
RDEPENDS_packagegroup-frick-network = "\
    nmap \
    vsftpd \
    netkit-ftp \
    openssh \
    openssh-sftp \
    openssh-sftp-server \
    "
RDEPENDS_packagegroup-frick-security = "\
    libcap \
    libcap-ng \
    libcap-bin \
    "

RDEPENDS_packagegroup-frick-ui-libs = "\
    libx11 \
    libx11-dev \
    libx11-xcb \
    libx11-doc \
    libxext \
    xextproto-dev \ 
    libxrandr \
    libxrender \
    libexif \
    libxi \
    libxscrnsaver \ 
    qt-libs \
"

