#!/usr/bin/env bash
set -eu

LAYERS_DIR=$(pwd)/sources/base/conf
FRICK_COMMENT="# Frick userspace layer"
LAYERS='\
  ${BSPDIR}/sources/meta-openembedded/meta-gnome \
  ${BSPDIR}/sources/meta-openembedded/meta-efl \
  \
  ${BSPDIR}/sources/openembedded-core/meta \    
  ${BSPDIR}/sources/openembedded-core/meta-skeleton \    
  \
  ${BSPDIR}/sources/meta-frick \
  ${BSPDIR}/sources/meta-browser \  
  ${BSPDIR}/sources/meta-golang \  
  ${BSPDIR}/sources/meta-qt4 \  
  \
  ${BSPDIR}/sources/meta-openembedded/meta-perl \
  ${BSPDIR}/sources/meta-openembedded/meta-python \
  ${BSPDIR}/sources/meta-openembedded/meta-networking \
  ${BSPDIR}/sources/meta-security \
'
LAYER_FRICK_STRING="\
${FRICK_COMMENT}
BBLAYERS += \"${LAYERS}\""
LICENSE_FLAGS_STRING="LICENSE_FLAGS_WHITELIST += \"commercial\""


if [ -w ${LAYERS_DIR}/bblayers.conf ]; then
    OUTPUT=""
    if grep "${FRICK_COMMENT}" "${LAYERS_DIR}/bblayers.conf" > /dev/null; then
        OUTPUT="already present"
    else
        echo -e "\n${LAYER_FRICK_STRING}" >> ${LAYERS_DIR}/bblayers.conf
        OUTPUT="added"
    fi
    echo "Layer meta-frick ${OUTPUT}"

    if ! grep "${LICENSE_FLAGS_STRING}" "${LAYERS_DIR}/bblayers.conf" > /dev/null; then
        echo -e "\n${LICENSE_FLAGS_STRING}" >> ${LAYERS_DIR}/bblayers.conf
    fi

fi




