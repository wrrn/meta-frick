# frick-image notes

## FTP
On a development machine a user will be able to use ftp with ```root```

## SSH
On a development machine a user will be able to use ssh with ```root```

## Wallpaper
To set the wallpaper you must create a symlink at one of the following locations: 

* ```/usr/share/wallpaper/wallpaper```
* ```$HOME/.config/wallpaper/wallpaper```

The symlink should point to the file you want to display as you wallpaper.

```sh
mkdir -p $HOME/.config/wallpaper
ln -s /home/frick/hd-background.jpg $HOME/.config/wallpaper/wallpaper
```
