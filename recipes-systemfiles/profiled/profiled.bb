# recipe to add script file extendpath.sh to /etc/profile.d that extend the PATH variable

SUMMARY = "PATH Variable Extender"
DESCRIPTION = "PATH Variable Extender"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=0636e73ff0215e8d672dc4c32c317bb3"

SRC_URI = " \
	file://extendpath.sh \
	file://COPYING \
    "

S = "${WORKDIR}"

do_install() {
	install -d ${D}/etc/profile.d/
	install -m 755 ${WORKDIR}/extendpath.sh ${D}/etc/profile.d/
}

FILES_${PN} = " \
	/etc/profile.d/extendpath.sh \
"
