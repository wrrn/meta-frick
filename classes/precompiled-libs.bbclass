# List of files that can be found in ${B}
# that are precompiled .so files
COMPILED_LIBS ?= ""
FILES_${PN}_append = " ${@compiled_libs(d)} ${@lib_links(d)}"

# lib_dir is a convience function that returns a string ${lib_dir} with a
# forward slash (/) appended to it
def lib_dir():
        return "${libdir}/"

# each_lib takes a bitbake datastore and a function and applies the function to
# the libs stored in the COMPILED_LIBS variable
def each_lib(d, fn):
    libs = d.getVar("COMPILED_LIBS", False)
    if libs is None:
        return
    
    for lib in libs.split(" "):
        fn(lib)

# each_link takes a bitbake datastore and a function that takes a library, and a
# link that references it and applies the function to the lib and its associated
# links stored in the LINKS variable.
def each_link(d, fn):
    linksdict = d.getVarFlags("LINKS")
    if linksdict is None:
        return
    
    for (lib, links) in linksdict.items():
        for link in links.split(" "):
            fn(lib, link)

# compiled_libs takes a bitbake datastore returns a space delimited string with
# all the libraries with ${libdir}/ prefixed to each library.
def compiled_libs(d):
    paths = [];
    each_lib(d, lambda lib: paths.append(lib_dir() + lib))
    return " ".join(paths)

# lib_links takes a bitbake datastore returns a space delimited string with
# all the links with ${libdir}/ prefixed to each of them.
def lib_links(d):
    paths = []
    each_link(d, lambda lib, link: paths.append(lib_dir() + link))
    return " ".join(paths)

# is_lib takes a filename and a list of patterns and returns 0 if the filename
# matches any of the patterns
is_lib() {
   local lib="$1"
   shift 1;
   for pat in "$@"; do
       if [ -z "${lib##$pat}" ]; then
           return 0
       fi
   done
   return 1
}

# libs_in_src returns a space delimited string of all the filenames in ${SRC_URI}
# that matches at least one of patterns passed into the function
libs_in_src() {
    local libs=""
    for f in ${SRC_URI}; do
        if is_lib $f "$@"; then
            libs="$libs ${f#*://}"
        fi
    done
    echo "$libs"
}

# install_libs installs all the libraries stored in ${COMPILED_LIBS}
install_libs() {
    install -d "${D}${libdir}"
    for lib in ${COMPILED_LIBS}; do
        install "${B}/$lib" "${D}${libdir}"
    done
}
    
do_install() {
    echo "COMPILED_LIBS = ${COMPILED_LIBS}"
    install_libs
}

# do_linklibs creates all the symlinks for each of the libraries
python do_linklibs() {
    import oe.path
    b = d.getVar("D", True) + d.getVar("libdir", True)
    os.chdir(b)
    each_link(d, lambda lib, link: oe.path.symlink(lib, link, True)) 
}

addtask linklibs after do_install before do_package





