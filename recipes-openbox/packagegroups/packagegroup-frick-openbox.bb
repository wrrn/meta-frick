SUMMARY = "openbox is a lightweight window manager"
DESCRIPTION = "openbox with xterm"
LICENSE = "MIT"


inherit packagegroup

PACKAGES = "\
    packagegroup-frick-openbox \
    "

RDEPENDS_packagegroup-frick-openbox = "\
    openbox \
    xterm \
    feh \
    wallpaper \
"
