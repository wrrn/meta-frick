SUMMARY = "Set a wallpaper for openbox"
DESCRIPTION = "Set a wallpaper for openbox"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=ef8f2e92f6fc127dd1309e2cca2fa82e"

PACKAGES = "${PN}"

DEPENDS = "openbox"
RDEPENDS_${PN} = "openbox feh"

SRC_URI = "\
    file://LICENSE \
    file://hd-desktop.jpg \
    "

FILES_${PN} = "${datadir}/wallpaper/*"

S = "${WORKDIR}"

do_install() {
     wallpaper_dir="${D}${datadir}/wallpaper"
     echo "wallpaper_dir = ${wallpaper_dir}"
     install -d "${wallpaper_dir}/"
     install "${B}/hd-desktop.jpg" "${wallpaper_dir}/"
     ln -s ${datadir}/wallpaper/hd-desktop.jpg ${wallpaper_dir}/wallpaper
}
