SUMMARY = "Fast and light imlib2-based image viewer"
DESCRIPTION = "Fast and light imlib2-based image viewer"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=a4a0ec75fdbfc5f496ea086eda7ad7d6"

DEPENDS = "libxt imlib2"
RDEPENDS_${PN} += "imlib2 imlib2-loaders libpng libx11"

SRC_URI = "https://feh.finalrewind.org/${PN}-${PV}.tar.bz2"
SRC_URI[md5sum] = "02c1892de5fae14e19a8c6be6afc421f"
SRC_URI[sha256sum] = "34be64648f8ada0bb666e226beac601f972b695c9cfa73339305124dbfcbc525"

inherit autotools

EXTRA_OEMAKE = "curl=0 xinerama=0 PREFIX=${prefix}"
EXTRA_OEMAKE_${PN}-dbg += "debug=1"

#autotools: broken build dir
B = "${S}"

FILES_${PN} += "${datadir}/icons/*"
