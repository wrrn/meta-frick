FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI += "\
    file://xterm-keyboard-shortcut.patch \
    file://xterm-only-menu.patch \
    file://feh-wallpaper-autostart.patch \
    "

ALTERNATIVE_TARGET[x-window-manager] = "${bindir}/openbox-session"