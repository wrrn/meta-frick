# recipes-openbox

## Packages

### openbox
openbox installs openbox and configures that following:

* Start openbox on boot
* CTRL+ALT+t opens the terminal
* Remove unnecessary entries from the application menu
* Try to start feh if it exists

### feh
feh installs the progam ```feh``` so that we can display a wallpaper.

### wallpaper
wallpaper adds the frick wallpaper, and sets up the necessary file structure to be able 
to display it.

### packagegroups
packagegroups provides the package group ```packagegroup-frick-openbox```.
