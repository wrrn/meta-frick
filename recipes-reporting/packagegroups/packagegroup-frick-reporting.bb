SUMMARY = "Libraries for reporting and system downloads"
DESCRIPTION = "xl, xls, and pdf libraries"
LICENSE = "MIT & LIBXL & BSD-3-Clause & GPLv1 & ZipArchive & LMX & GPLv1"

inherit packagegroup

PACKAGES = "\
    packagegroup-frick-reporting \
    "

RDEPENDS_packagegroup-frick-reporting = "\
    libxl \
    libxls-dev \
    libhpdf-dev \
    libhpdf-staticdev \
"