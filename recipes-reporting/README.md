# recipes-reporting
recipes-reporting contains all the libraries Denny needs to reporting.

## Packages
### reporting
reporting has packages that install libraries needed for reporting on the Quantum HD Prime Unity products.

#### Provided packages
##### libhpdf
Installs ```libhpdf.so.2.4.0.dev``` and ```libhpdf.a``` libraries, and adds ```libhpdf.so``` and  ```libhpdf-2.4.0dev.so``` as symlinks to ```libhpdf.so.2.4.0.dev```.

##### libxls
Installs ```libxls.so.3.0.0``` library, and adds ```libxls.so.3``` and  ```libxls.so``` as symlinks to ```libxls.so.3.0.0```.

##### libxl
Installs ```libxl.so``` library.

### packagegroups
packagegroups provides the package group ```packagegroup-frick-reporting```
