SUMMARY = "libHaru is a free, cross platform, open source library for generating PDF files"
DESCRIPTION = "libHaru is a free, cross platform, open source library for generating PDF files"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=6e8c56b45b59650a1557236931043b1e"

DEPENDS = "zlib"

SO_FILE = "libhpdf.so.2.4.0dev"
LIBHPDF_STATIC = "libhpdf.a"

SRC_URI = "\
    file://LICENSE \
    file://${SO_FILE} \
    file://${LIBHPDF_STATIC} \
"
S = "${WORKDIR}"

require recipes-precompiled-libs/utils/downgrade-errors.inc
inherit precompiled-libs
COMPILED_LIBS = "${SO_FILE} ${LIBHPDF_STATIC}"
LINKS[libhpdf.so.2.4.0dev] = "libhpdf.so libhpdf-2.4.0dev.so"


