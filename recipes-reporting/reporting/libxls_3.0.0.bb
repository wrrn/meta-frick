SUMMARY = "Another C++ Excel Library to read/write xls/xlxs files"
DESCRIPTION = "Another C++ Excel Library to read/write xls/xlxs files"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=0636e73ff0215e8d672dc4c32c317bb3"
SO_FILE = "libxls.so.3.0.0"

SRC_URI = "\
    file://COPYING \
    file://${SO_FILE} \
    "

S = "${WORKDIR}"


require recipes-precompiled-libs/utils/downgrade-errors.inc
inherit precompiled-libs
COMPILED_LIBS = "${SO_FILE}"
LINKS[libxls.so.3.0.0] = "libxls.so.3 libxls.so"


