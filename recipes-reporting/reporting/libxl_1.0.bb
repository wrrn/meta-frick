SUMMARY = "A C++ Excel Library to read/write xls/xlxs files"
DESCRIPTION = "A C++ Excel Library to read/write xls/xlxs files"
LICENSE = "LIBXL & BSD-3-Clause & GPLv1 & ZipArchive & LMX"
LIC_FILES_CHKSUM = "file://license.txt;md5=5601ef6ecd5fc4ef677d8a06ae7590c5"

SRC_URI = "\
    file://license.txt \
    file://libxl.so\
    "
# Tells bitbake that it shouldn't automatically create
# .so symlinks
FILES_SOLIBSDEV = ""

require recipes-precompiled-libs/utils/downgrade-errors.inc
inherit precompiled-libs

COMPILED_LIBS = "libxl.so"


S = "${WORKDIR}"
