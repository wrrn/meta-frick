SUMMARY = "QT shared libraries"
DESCRIPTION = "QT shared libraries"
LICENSE = "GPLv3 & LGPL"
LIC_FILES_CHKSUM = "\
    file://LICENSE.GPL3;md5=6eb76854907e4134178ac22d53b010cd       \
    file://LICENSE.LGPL;md5=fbc093901857fcd118f065f900982c24       \
    file://LGPL_EXCEPTION.txt;md5=411080a56ff917a5a1aa08c98acae354 \
"
DEPENDS = "libxext libx11 fontconfig zlib freetype" 

SRC_URI = "\
    file://LICENSE.GPL3       \
    file://LICENSE.LGPL       \
    file://LGPL_EXCEPTION.txt \
    \
    file://libQtCore.so.4     \
    file://libQtGui.so.4.6    \
    file://libQtSql.so.4      \
    "

S = "${WORKDIR}"


require recipes-precompiled-libs/utils/downgrade-errors.inc
inherit precompiled-libs

COMPILED_LIBS = "$(libs_in_src '*.so.*')"







