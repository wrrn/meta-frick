# recipes-qt-libs
## Packages
### qt-libs
Some precompiled Qt 4.6 libraries that Denny wants in frick-image. It
contains the following Qt libraries.

* libQtCore
* libQtGui
* libQtSql
