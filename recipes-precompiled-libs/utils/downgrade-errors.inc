# Remove Errors for not having symlinks and .so's stripped of
# debug symbols
ERROR_QA_remove = "dev-so already-stripped dev-elf"

# Add warning for not having symlinks and .so's stripped of
# debug symbols
WARN_QA += "dev-so already-stripped dev-elf"

