Meta-frick
==========

This layer provides the necessary packages required to build a Poky image for 
the Frick Prime Unity products.

Dependencies
============

This layer depends on:
```
  URI: https://github.com/OSSystems/meta-browser
  branch: krogoth
  revision: HEAD
  prio: default

  URI: git://git.openembedded.org/openembedded-core
  branch: krogoth
  revision: HEAD
  prio: default

  URI: git://git.openembedded.org/meta-openembedded/meta-oe
  branch: krogoth
  revision: HEAD
  prio: default

  URI: git://git.openembedded.org/meta-openembedded/meta-networking
  branch: master
  revision: HEAD
  prio: default
  
  URI: git://git.openembedded.org/meta-openembedded/meta-security
  branch: master
  revision: HEAD
  prio: default
  
  URI: git://git.openembedded.org/meta-openembedded/meta-efl
  branch: krogoth
  revision: HEAD
  prio: default
  
  URI: git://git.openembedded.org/meta-openembedded/meta-perl
  branch: krogoth
  revision: HEAD
  prio: default
  
  URI: git://git.openembedded.org/meta-openembedded/meta-python
  branch: krogoth
  revision: HEAD
  prio: default
```

## Recipes

### [recipes-frick](recipes-frick/README.md)
### [recipes-daemons](recipes-daemons/README.md)
### [recipes-openbox](recipes-openbox/README.md)
### [recipes-precompiled-libs](recipes-precompiled-libs/README.md)
### [recipes-qt-libs](recipes-qt-libs/README.md)
### [recipes-reporting](recipes-reporting/README.md)
### [recipes-systemfiles](recipes-systemfiles/README.md)

## Adding a package to frick-image
To add a package to frick-image you can to add it to one of the packagegroups in
the file ```recipes-frick/packagegroups/packagegroup-frick.bb```.

---


## Adding a precompiled library
Please note that this is not Yocto expects a user to do.
We are going to be adding the ```libQtCore.so.4``` library in this example.

### Structure
Assuming that we have the files ```libQtCore.so.4``` and ```LICENSE.GPL3```, we
will need to copy then into ```recipes-qt-libs/lib-qt-core```. Our structure should
look like this:

```
recipes-qt-libs/
└── lib-qt-core/
    ├── libQtCore.so.4
    └── LICENSE.GPL3
```

### Create lib-qt-core\_4.0.bb
Create a file with the name ```lib-qt-core_4.0.bb``` and place it in the
directory ```recipes-qt-libs```.  You will need to fill out the ```SUMMARY```
and ```DESCRIPTION``` variables.

```
SUMMARY = "Lib QT core"
DESCRIPTION = "The core library for QT"
```

Our directory structure will be as follows:

```
recipes-qt-libs/
├── lib-qt-core/
│   ├── libQtCore.so.4
│   └── LICENSE.GPL3
└── lib-qt-core_.4.0.bb
```


### License
Now we need to take care of licensing. Yocto will not compile if the licensing
is not specified correctly.

#### LICENSE variable
You must add the ```LICENSE``` variable and specify the license type. Yocto has
a list of common licenses in ```yocto/meta/files/common-licenses```.

```
LICENSE = "GPLv3"
```

#### LIC\_FILE\_CHKSUM variable
Yocto requires that you provide a checksum for the LICENSE file so that it can
verify that the LICENSE hasn't changed. You can generate the md5 sum using the
```md5sum`` command line tool. 

```sh
md5sum recipes-qt-libs/lib-qt-core/LICENSE.GPL3
# Output: 6eb76854907e4134178ac22d53b010cd
```

We set our ```LIC_FILES_CHKSUM``` variable:

```
LIC_FILES_CHKSUM = "\
     file://LICENSE.GPL3;md5=6eb76854907e4134178ac22d53b010cd       \
     "
```

#### SRC\_URI
We now have to tell bitbake to copy the file into its worko directory. To do
this we add the file to the ```SRC_URI``` variable.

```
SRC_URI = "\
    file://LICENSE.GPL3       \
    "
```

With the ```file://``` prefix bitbake will look in the ```lib-qt-core``` directory for
the file specified after it.

### Copy the ```.so``` file into the work direcory
We now need to tell Yocto to copy over ```libQtCore.so.4``` into the work
directory. To do this we just add ```libQtCore.so.4``` to the ```SRC_URI```
variable. 

```
SRC_URI = "\
    file://LICENSE.GPL3       \
    file://libQtCore.so.4     \
    "
```

### Set the source directory to point at the work directory.
The source directory is where Yocto looks for files that we specify. We have
told Yocto to copy the files the work directory. Now we must tell Yocto that the
files are in the work directory. To do that we just set the variable ```S``` 
to ```${WORKDIR}```

```
S = ${WORKDIR}
```

### Downgrade Errors
Yocto will fail because the files are missing debugging information and the .so
symbolic links files should be in the ```-dev``` packages. To downgrade the
errors we need to include the ```downgrade-errors.inc``` file.

```
require recipes-precompiled-libs/utils/downgrade-errors.inc
```

### Inherit precompiled-libs
We will need to inherit the functionality to add a precompiled library to
Yocto.

```
inherit precompiled-libs
```

### Add COMPILED\_LIBS
We need to tell Yocto what files should be considered a precompiled library. To
do this we add the file name to the ```COMPILED_LIBS``` variable.

```
COMPILED_LIBS = "libQtCore.so.4"
```

### Specify symlinks
We can specify any symlinks that need to present in ```/usr/lib``` by adding the
library to the ```LINKS``` as variable flag and setting the value of the flag to
whatever the symlinks to be.

```
LINKS[libQtCore.so.4] = "libQtCore.so libQtCore.4.so"
```

### Add package to the frick-image
Now we need to add the package to frick-image. We need to add the name of our
package to the ```frick-ui-libs``` package group in
```recipes-frick/packagegroups/packagegroup-frick.bb```. We modify the
```RDEPENDS_packagegroup-frick-ui-libs``` variable by appending
```lib-qt-core \``` to it. 

```
RDEPENDS_packagegroup-frick-ui-libs = "\
    libx11 \
    libx11-dev \
    libx11-xcb \
    libx11-doc \
    libxext \
    xextproto-dev \ 
    libxrandr \
    libxrender \
    libexif \
    libxi \
    libxscrnsaver \ 
    lib-qt-core \       <--- We added that line
"
```

---


## Classes

### precompiled-libs.bbclass
The precompiled-libs class supports adding a precompiled library to the Yocto build.

After you inherit precompiled-libs you will be able to tell Yocto what files
and associated symbolic links should be included in ```/usr/lib```.

#### Key Variables

##### COMPILED_LIBS
```COMPILED_LIBS``` is a space delimited string that specifies what files in the
```WORKDIR``` will be installed to ```/usr/lib``` of your image.

##### LINKS
```LINKS``` specifies the symbolic links that will be installed in
```/usr/lib```. To specify the links for a specific library create a variable
flag for that library. The value of the variable flag needs to be a space
delimited list of symbolic links associated with the aformentioned library. Note
that that library used as the variable flag must be in the ```COMPILED_LIBS``` list.

Example for libext:

```
COMPILED_LIBS = "libext.so.4"
LINKS[libext.so.4] = "libext.so libext.4.so"
```


#### Helper functions

##### ```libs_in_src```
```libs_in_src``` returns a space delimited string of all the filenames in
${SRC_URI} that matches at least one of patterns passed into the function with
the ```file://``` prefix removed.

---

# Notes for frick-image

## FTP
On a development machine a user will be able to use ftp with ```root``` and the
password ```frick```.

## SSH
On a development machine a user will be able to use ssh with ```root``` and the
password ```frick```.

## Wallpaper
To set the wallpaper you must create a symlink at one of the following locations: 

* ```/usr/share/wallpaper/wallpaper```
* ```$HOME/.config/wallpaper/wallpaper```

The symlink should point to the file you want to display as you wallpaper.

```sh
mkdir -p $HOME/.config/wallpaper
ln -s /home/frick/hd-background.jpg $HOME/.config/wallpaper/wallpaper
```









